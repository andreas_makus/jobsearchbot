"""
This is the main script to run to check the sites for new job offers.
The basic workflow is as follows:

- Create logger to log status information
- Open Browser
- Log into email account (Gmail)
- Get site html
- Scrape information
- Analyze and compare job entries to database
- Send email with added or removed positions

Author: Andreas Makus
"""

import os
import logging
import sys
import argparse
import selenium

from config import config, log, defaults

from sample.retrieve import Retriever
from sample.scrape import Scraper
from sample.analyze import Analyzer
from sample.email import Mailer
from sample import toolshed as ts

# configure logger instance
logger = log.configure_logger(config.LOGGER)
log.log_blank_line()


def main(websites=None, sender=None, password=None, receivers=None):
    """
    Main function that contains the complete script.

    :return:
    """

    # initiate logger and browser
    logger.info("Script initiated for the following websites: \n%s", "\n".join(websites))

    # instantiate main classes
    retriever, scraper = Retriever(), Scraper(),
    analyzer = Analyzer(websites)
    mailer = Mailer(sender, password, receivers)

    # start mongo
    try:
        analyzer.run_mongodb()
    except (FileNotFoundError, NotADirectoryError) as err:
        logger.exception("Could not run MongoDB, exiting script")
        sys.exit()

    # start browser
    try:
        retriever.start_browser()
    except Exception:  # TODO: add exceptions
        logger.exception("Could not run browser, exiting script")
        sys.exit()

    # scrape websites and data job information in dict
    scraped_jobs = []
    for site in websites:

        # get html of site, url_group to differentiate sites
        try:
            url_group, html_soup = retriever.retrieve_site_html(site)
        except NotImplementedError as exc:
            logger.error("Website is not yet implemented, or does not belong to a site group: %s", site)
            continue
        except IndexError as err:
            logger.error(">>>>> No jobs were retrieved for site <<<<<")
            continue

        # scrape jobs from html soup
        scraped_jobs += scraper.scrape_site(html_soup, url_group)

    # quit browser
    retriever.quit_browser()

    # make sure that job entries are unique
    scraped_jobs = ts.make_list_unique(scraped_jobs)

    # compare scraped jobs to the ones in db
    stored_jobs = analyzer.get_jobs_from_db()
    # store_file, stored_jobs = analyzer.get_stored_jobs(websites)
    email_content = analyzer.get_new_removed_jobs_by_group(scraped_jobs, stored_jobs)

    # send new jobs to email
    try:
        # check all groups for differences
        for group in email_content:
            # if any difference is detected, send email
            if any(v for k,v in email_content[group].items()):
                mailer.send_email(websites, email_content)
                break
        else:
            logger.info("No changes detected; no Email sent")

    except Exception as exc: # TODO: add exception for email fails
        logger.exception("Could not send email! Please incorporate changes")
    else:
        # write jobs to file (overwrite old entries) only when emails sent
        analyzer.write_jobs_to_store(store_file, websites, scraped_jobs)

    return


if __name__ == "__main__":

    # parse variables; this is where the required arguments are defined
    parser = argparse.ArgumentParser()
    parser.add_argument("--account", required=True)
    parser.add_argument("--password")
    parser.add_argument("--password-from-file")
    parser.add_argument("--receivers", nargs="+", required=True)
    parser.add_argument("--websites", nargs="*", default=[defaults.DEF_WEBSITE])
    parser.add_argument("--websites-from-file")
    args = parser.parse_args()

    # process input: password
    if args.password:
        pw = args.password
    else:
        try:
            pw = ts.get_pw_from_file(args.password_from_file, args.account)
        except TypeError:
            logger.error("Must provide either password or password file. Exiting script")
            sys.exit()
        except FileNotFoundError:
            logger.error("Could not find password file in %s. Exiting script", args.password_from_file)
            sys.exit()
        else:
            if pw == None:
                logger.error(f"Password not found for {args.account} provided. Exiting script")
                sys.exit()

    # process input: websites
    if args.websites_from_file:
        try:
            sites = ts.get_websites_from_file(args.websites_from_file)
        except FileNotFoundError:
            logger.error("Could not find websites file %s", args.websites_from_file)
        except TypeError:
            logger.error("--websites-from-file must be string; can not find websites")
    else:
        sites = args.websites
        logger.info("Using DEFAULT websites")

    # initiate main function using variables
    main(sites, args.account, pw, args.receivers)


# TODO: add error handling if site is down/not reachable
# TODO: check if job sites can be added
# TODO: add MongoDB database
# TODO: if no jobs scraped, notify user! >> this should work; check if it does!