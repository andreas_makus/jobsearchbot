This project contains a script that allows the user to specify a website to scrape for new and removed job entries.
The script loads the HTML of the given websites and scrapes their job information.
If new jobs are present or removed, an email is sent to all listed receivers with the affected jobs.
Jobs are compared to stored jobs in the database (JSON file) and grouped by websites. If the user changes
the combination of websites to search, a new database is created and used for comparing jobs using those websites.

Example use:
~~~
>> python path/to/main.py -sender [email] -password [password] -receivers [receivers] -websites [websites]
~~~

where:

  * sender is a string argument that must be a gmail account.
  * password is a string containing the password for said account.
  * receivers is a list of strings (emails).
  * website is a list of strings with websites to scrape.

The basic workflow is as follows:

  * Create logger to log status information
  * Create Browser instance and open Browser
  * Log into email account (Gmail)
  * Loop through websites and get site html
  * Scrape site information
  * Analyze and compare job entries to database
  * Send email with added or removed positions

Available sites (so far) are:

  * "DLR"
  * "AIRBUS"