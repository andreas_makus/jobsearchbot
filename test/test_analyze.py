import unittest
import copy
from pymongo import MongoClient

from sample.analyze import Analyzer
from test.test_scrape import Scraper_Test


class Analyzer_Test(unittest.TestCase):
    """"""

    TEST_SITE = ["https://www.test-test-test.com"]


    def test_mongo_open_close(self, websites=TEST_SITE):
        """"""
        analyzer = Analyzer(websites)
        analyzer.run_mongodb()
        collection = analyzer.db['test_collection_1']
        collection.insert_one({'something new': 'some data'})
        collection.drop()
        analyzer.quit_mongodb()

        return


    def test_save_to_retrieve_from_db(self, websites=TEST_SITE):
        """"""
        scraped_jobs = Scraper_Test().test_scrape_FERCHAU()
        analyzer = Analyzer(websites)
        analyzer.run_mongodb()
        analyzer.write_jobs_to_db(copy.deepcopy(scraped_jobs))
        stored_jobs = analyzer.get_jobs_from_db()
        assert all(job in stored_jobs for job in scraped_jobs)
        analyzer.db[analyzer.collection_name].drop()
        analyzer.quit_mongodb()

        return