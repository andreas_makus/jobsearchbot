import os
import unittest

from config import config, test
from sample import toolshed as ts
from sample.retrieve import Retriever
from sample.scrape import Scraper


class Scraper_Test(unittest.TestCase):

    FERCHAU_site = "https://www.ferchau.com/de/de/karriere/jobs-bewerbung/jobangebote/search/technik/luft-und-raumfahrttechnik/hamburg+25km/"

    def test_scrape_FERCHAU(self, site=FERCHAU_site):

        # load url group
        url_group = Retriever().get_url_group(site)
        assert url_group == "FERCHAU"

        # load exmaple html
        path_to_example = os.path.normpath(test.EXAMPLE_PATHS[url_group])
        html_soup = ts.html_file_to_soup(path_to_example)

        # scrape jobs from html soup
        scraper = Scraper()
        jobs_info = scraper.scrape_site(html_soup, url_group)
        assert jobs_info

        return jobs_info
