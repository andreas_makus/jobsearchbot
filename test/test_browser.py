import unittest

# from sample.browser import Browser
from sample.retrieve import Retriever


class Browser_Test(unittest.TestCase):

    def test_open_close_browser(self):
        retriever = Retriever()
        retriever.start_browser()
        retriever.quit_browser()
