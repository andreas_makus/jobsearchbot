from config import config
import subprocess


ACCOUNT = "py.makus@gmail.com"
RECEIVERS = "py.makus@gmail.com"
PW_FILE = r"C:\Users\Andreas\Desktop\google_accounts.txt"
SITES = r"C:\Users\Andreas\Desktop\check_jobs.txt"

command = f"python {config.MAIN_FILE} --account {ACCOUNT} --password-from-file {PW_FILE} --receivers {RECEIVERS} --websites-from-file {SITES}"
subprocess.run(command)

