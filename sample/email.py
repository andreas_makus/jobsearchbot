import smtplib, logging
import sys
from config import config, defaults
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# get logger
logger = logging.getLogger(config.LOGGER)

class Mailer():
    """
    Class that encompasses all functions necessary to send the parsed html content via email.

    When using your google account to send emails, the "insecure apps" permission must be enabled.

    """
    def __init__(self, sender, password, receivers):
        self.receivers = receivers
        self.sender = sender
        self.password = password

        # log in with email credentials
        self.login_gmail()

    def login_gmail(self):
        """
        Logs in with provided credentials.

        :return:
        """
        try:
            self.mail = smtplib.SMTP('smtp.gmail.com', 587)
            self.mail.ehlo()
            self.mail.starttls()
            self.mail.login(self.sender, self.password)
        except smtplib.SMTPAuthenticationError as err:
            logger.error("Could not log into GMAIL using the provided credentials.")
            self.exit_script()

        return


    def send_email(self, websites, body_information):
        """
        Main function to send emails with. Uses sender/receiver attributes from the class; needs jobs information to
        generate email body.

        :return:
        """
        """
        The content of this function is based on: 
        https://stackoverflow.com/questions/882712/sending-html-email-using-python
        """
        logger.info("Preparing email")

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Automated Job Search"
        msg['From'] = self.sender
        msg['To'] = ", ".join(self.receivers)

        # Create the body of the message (a plain-text and an HTML version).
        html = self.create_email_body(websites, body_information)

        # Record the MIME types of both parts - text/plain and text/html.
        mime_text = MIMEText(html, 'html')

        # Attach parts into message container.
        msg.attach(mime_text)

        self.mail.sendmail(self.sender, ", ".join(self.receivers), msg.as_string())
        self.mail.quit()

        logger.info("Email was sent successfully to %s", self.receivers)
        return


    def create_email_body(self, websites, body_information):
        """
        Creates email body by using the parsed information in form of a dictionary.

        :param body_information:
        :return:
        """
        html_doc = """"""
        html_doc += self.create_html_header()
        html_doc += self.create_website_info_html(websites)
        for site in body_information:
            site_html = f"<h2><u>{site.upper()}</u></h2>"
            for k in body_information[site].keys():
                if not body_information[site][k]:
                    continue
                else:
                    site_html += f"<h3>{k.capitalize()}({len(body_information[site][k])}):</h3>"
                    for job in body_information[site][k]:
                        site_html += self.dict_to_html_table(job) + "</br>"

            if "h3" in site_html:
                html_doc += site_html

        html_doc += "</body></html>"

        logger.debug("Email body created successfully")
        return html_doc


    @staticmethod
    def create_website_info_html(websites):
        """
        Returns html string with info on which websites have been expected.

        :param websites:
        :return:
        """
        sites_html = "<ul>"
        for site in websites:
            sites_html += f"<li><a href='{site}'>{site}</a></li>"
        else:
            sites_html = sites_html + "</ul>"

        return f"<p> The following URLs were searched for changes in job postings: <br/>{sites_html}</p>"


    @staticmethod
    def create_html_header():

        table_set = "table {" \
                    + "font-family: arial, sans-serif; border-collapse: collapse;"\
                    + f" width: {defaults.TABLE_WIDTH}%; margin-bottom: {defaults.TABLE_BOTTOM_MARGIN}px;"\
                    + "}"
        cell_set = "td, th {" + "text-align: left;" + f"padding: {defaults.CELL_PADDING}px;" + "}"
        paragrap_set = "p {" \
                       + f"display: block; margin-top: {defaults.P_MARGIN}em; margin-bottom: {defaults.P_MARGIN}em;" \
                       + "}"
        header = f"""<html><head><style> {table_set}{cell_set}{paragrap_set}</style></head><body>"""
        return header


    @staticmethod
    def dict_to_html_string(info_dict):
        """
        Uses dictionary with job information to return information for use in html p-tag.

        :param info_dict:
        :return:
        """

        info_string = ""
        for key,value in info_dict.items():
            if key == "url":
                info_string += f"{key}:&emsp;&emsp;<a>{value}</a><br/>"
            else:
                info_string += f"{key}:&emsp;&emsp;{value}<br/>"

        return info_string


    @staticmethod
    def dict_to_html_table(info_dict):
        """
        Takes a dictionary with all relevant job info and turns it into an html-table. Table is returned as string.

        :param info_dict:
        :return:
        """
        job_table = """<table frame="box">"""
        for key, value in info_dict.items():
            if key == "url":
                job_table += f"<tr><td width=20%><b>{key.upper()}</b></td><a href={value}>link</a><td></td></tr>"
            else:
                job_table += f"<tr><td width=20%><b>{key.capitalize()}</b></td>{value}<td></td></tr>"
        job_table += "</table></br>"

        return job_table