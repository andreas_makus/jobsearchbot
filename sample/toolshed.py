import json
import sys
import csv
import logging
from bs4 import BeautifulSoup

def make_list_unique(li):
    """
    Returns list with unique elements.

    :param list:
    :return:
    """
    # convert dicts to json strings, check for uniqueness using set, convert back to dict
    dict_strings = list(set(json.dumps(x, sort_keys=True) for x in li))
    unique_list = [json.loads(x) for x in dict_strings]

    return unique_list


def remove_chars_from_str(string, *args):
    """
    Remove characters from string.

    :param string:
    :return:
    """

    for arg in args:
        if arg in string:
            string = string.replace(arg, "")
    return string


def replace_chars_in_str(string, *args):
    """
    Remove characters from string. Arguments must be tuples (replacee, replacer).

    :param string:
    :return:
    """

    for arg in args:
        if arg[0] in string:
            string = string.replace(arg[0], arg[1])
    return string


def html_file_to_soup(path_to_file):
    """
    This function parses file contents to an ElementTree object and returns its root.

    :param path_to_file:
    :return:
    """

    with open(path_to_file, "r", encoding="utf-8") as f:
        content = f.read()
        f.close()

    return BeautifulSoup(content, "html.parser")


def html_str_to_soup(html_string):
    """
    This function parses the html string using the etree html parser and returns its root element.

    :param html_string:
    :return:
    """
    return BeautifulSoup(html_string, "html.parser")


def get_pw_from_file(filepath, account):
    """
    Checks file for correct passowrd for corresponding google account.

    :param filepath:
    :param account:
    :return:
    """
    with open(filepath) as file:
        data = csv.reader(file, delimiter=" ")

        for line in data:
            if line[0] == account:
                return line[1]
        else:
            return


def get_websites_from_file(filepath):
    """
    Returns websites stored in file.

    :param filepath:
    :return:
    """
    with open(filepath) as file:
        data = csv.reader(file)

        return [x for b in data for x in b]

