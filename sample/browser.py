import logging
from config import config, defaults
from selenium import webdriver

# start logger
logger = logging.getLogger(config.LOGGER)

class Browser():
    """
    Browser class simplifies creating a browser and quitting it.

    """
    def __init__(self):
        self.browser = None


    def start_browser(self, browser_name):
        """
        Initiates browser to scrape web data. Config file determines which browser is used.

        :return:
        """

        # start browser according to arg
        if browser_name == "CHROME":
            self.browser = webdriver.Chrome(config.CHROME_PATH)
        elif browser_name == "PHANTOMJS":
            self.browser = webdriver.PhantomJS(config.PHANTOMJS_PATH, service_log_path=config.BROWSER_LOG)
        else:
            if not browser_name == "FIREFOX":
                logger.info(f"Could not find browser {browser_name}, starting FIREFOX")
            self.browser = webdriver.Firefox(executable_path=config.FIREFOX_PATH, log_path=config.BROWSER_LOG)
            browser_name = "FIREFOX"

        # make browser invisible; move outside of view
        if not config.BROWSER_VISIBLE:
            self.browser.set_window_position(-3000, 0)

        logger.info("Browser running: %s", browser_name)

        return self.browser


