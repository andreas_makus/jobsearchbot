import os
import logging

import selenium
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from sample import toolshed as ts
from config import config, defaults

# get logger
logger = logging.getLogger(config.LOGGER)


class Scraper():
    """
    This class provides all functions related to retrieving websites.
    """

    def __init__(self):

        self.REMOVECHARS = config.REMOVECHARS
        self.REPLACECHARS = config.REPLACECHARS
        self.IGNORE_IN_HTML = config.IGNORE_IN_HTML

        # --- HARDCODE ---

        # Switches for various functions
        self.scrape_functions = {"DLR": self.scrape_DLR,
                                 "AIRBUS": self.scrape_AIRBUS,
                                 "FERCHAU": self.scrape_FERCHAU}


    def scrape_site(self, html_soup, url_group):
        """
        Function to perform scraping operations on different sites; function selector.

        :param: html_soup: BeautifulSoup object; parsed html content
        :return: url_group: url-group of website
        :return: jobs_list: list of jobs collected in dictionaries
        """

        # scrape jobs from html
        scraped_jobs = self.scrape_functions[url_group](html_soup)
        if not scraped_jobs[0]: raise IndexError   # check if jobs list empty; raise error if so

        # convert to string; remove unwanted characters; 'url' keys are ignored
        for job_dict in scraped_jobs:
            for key, value in job_dict.items():
                value = str(value)
                if key is not "url":
                    job_dict[key] = ts.remove_chars_from_str(value, *self.REMOVECHARS)
                    job_dict[key] = ts.replace_chars_in_str(value, *self.REPLACECHARS)

        return scraped_jobs


    def scrape_DLR(self, html_soup):
        """
        This function scrapes the DLR website and returns the most important job info in a list of dicts.
        Here is where changes on the site must be implemented by changing the search terms.

        :param html_soup:
        :return:
        """
        # get parent node and jobs nodes
        jobs_node = html_soup.find("ul", {"class": "jobs-tiles"})

        # combine all jobs in dict
        jobs_list = []
        for job in jobs_node.children:
            if job in self.IGNORE_IN_HTML: continue # ignore certain nodes
            job_dict = {}

            # get jobs details; if not available, skip
            job_dict["company"] = "DLR"
            try:
                job_dict["type"] = job.find("p", {"class":"description_short"}).string
            except AttributeError: pass
            try:
                job_dict['title'] = job.find("h3", {"class":"title"}).string
            except AttributeError: pass
            try:
                job_dict['institute'] = job.find("span", {"class": "location"}).string
            except AttributeError: pass
            try:
                job_dict['url'] = job.find("a", {"class": "tile-padding"}).get("href")
            except AttributeError: pass

            if job_dict:
                jobs_list.append(job_dict)
            else:
                continue

        return jobs_list


    def scrape_AIRBUS(self, html_soup):
        """"""
        # get parent node for job listings
        results_node = html_soup.find("ul", {"class":"result-list"})

        # loop through parent and save all job listings to list
        jobs_list = []
        for job_node in results_node.children:

            # ignore spaces or tabs
            if job_node in self.IGNORE_IN_HTML: continue  # ignore certain nodes

            # get jobs details; if not available, skip
            job_dict = {}
            job_dict["company"] = "AIRBUS"
            try:
                job_dict["title"] = job_node.find("h2", {"class": "title"}).string
            except AttributeError: pass
            try:
                job_dict["category"] = job_node.find("span", {"class": "category"}).string
            except AttributeError: pass
            try:
                job_dict["department"] = job_node.find("span", {"class": "department"}).string
            except AttributeError: pass
            try:
                job_dict["location"] = job_node.find("span", {"class": "location"}).string
            except AttributeError: pass
            try:
                job_dict["url"] = "http://company.airbus.com" + job_node.find("a", {"class": "link-box"}).get("href")
            except AttributeError: pass

            if job_dict:
                jobs_list.append(job_dict)
            else:
                continue

        return jobs_list


    def scrape_FERCHAU(self, html_soup):
        """"""

        # get parent node for job listings
        results_node = html_soup.find("div", {"id": "resultbox"})

        # loop through parent and save all job listings to list
        jobs_list = []
        for job_node in results_node.children:
            if job_node in self.IGNORE_IN_HTML: continue  # ignore certain nodes
            job_dict = {}
            job_dict["company"] = "FERCHAU"
            # get jobs details; if not available, skip
            try:
                a = job_node.find("h3").find("a")
                job_dict["title"] = [_ for _ in a.stripped_strings]
            except AttributeError: pass
            try:
                job_dict["location"] = job_node.find("span", {"class":"site"}).string
            except AttributeError: pass
            try:
                job_dict["url"] = "https://www.ferchau.com" + job_node.find("h3").find("a").get("href")
            except AttributeError: pass

            # make sure title is a string without locations
            if job_dict["location"] in job_dict["title"]:
                job_dict["title"].remove(job_dict["location"])

            # concatinate title strings
            job_dict["title"] = " ".join(job_dict["title"])

            if job_dict:
                jobs_list.append(job_dict)
            else:
                continue

        return jobs_list











