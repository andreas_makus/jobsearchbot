"""
This file contains functions to analyze web data that has been scraped, all collected in the class Analyzer.
"""
import json
import os
import logging
import subprocess
import hashlib

from config import config
from datetime import datetime
from pymongo import MongoClient

# get logger
logger = logging.getLogger(config.LOGGER)

class Analyzer():
    """
    Class that analyzes and compares scraped jobs. Uses JSON format for local storage file.

    """
    def __init__(self, websites):
        self.path_to_store = config.STORE_DIR
        self.collection_name = self.make_collection_hash(websites)


    def make_collection_hash(self, websites):
        """
        Function create a hash for the db collection for a certain combination of websites. Each combination has a
        separate collection so that the sites can be observed properly over the long term.

        :param websites:
        :return:
        """
        # sort, concetanate, hash websites
        sites_string = "".join(sorted(websites))
        sites_hash = hashlib.sha224(sites_string.encode("utf-8")).hexdigest()

        return config.COLLECTION_PREFIX + sites_hash[:config.HASH_SLICE]


    def run_mongodb(self, db=config.DATABASE):
        """"""

        # check if mongo path exists
        if not os.path.exists(config.MONGODB):
            raise FileNotFoundError

        # check if database folder exists
        if not os.path.isdir(config.DATABASE_DIR):
            raise NotADirectoryError

        # taken from: https://stackoverflow.com/questions/29565288/opening-mongod-within-python-how-to-avoid-shell-true
        self.mongod = subprocess.Popen([config.MONGODB, '--dbpath', os.path.expanduser(config.DATABASE_DIR)])
        self.client = MongoClient()
        self.db = self.client[db]

        return


    def quit_mongodb(self):
        """"""
        # kill and close (both needed, otherwise error)
        self.mongod.terminate()
        self.mongod.kill()

        return


    def write_jobs_to_db(self, jobs):
        """"""
        coll= self.db[self.collection_name]
        added = coll.insert_many(jobs)
        logger.debug("Jobs added to %s: %s", coll.name, len(added.inserted_ids) )

        return


    def get_jobs_from_db(self):
        """"""
        return [doc for doc in self.db[self.collection_name].find({}, {'_id': False})]


    def get_stored_jobs(self, websites):
        """
        This function retrieves the stored jobs for the given websites. If no data exists for a given set of websites,
        a new data file is created.

        :return:
        """

        # loop through data dir; check if websites match (must be same to use same data file)
        for obj in os.listdir(self.path_to_store):
            obj_path = os.path.join(self.path_to_store, obj)

            try:
                with open(obj_path) as data_file:
                    data = json.load(data_file)

                    # check matching websites
                    if set(data["header"]["websites"]) == set(websites):  # compare websites using sets
                        store_file = obj_path
                        content = data["content"]
                        logger.debug("Using the following file for jobs comparison: %s", obj_path)
                        break  # if match, use that storefile

            except IOError:
                logger.exception("Could not open file: %s", obj_path)

        else: # if no matching websites found, empty file is created
            logger.debug(f"Could not find data file with matching websites in {self.path_to_store}")
            store_file = self.create_empty_store_file(self.path_to_store)
            content = {}
            logger.debug(f"New data file created: {store_file}")

        return store_file, content


    def create_empty_store_file(self, path_to_store):
        """
        Creates a data file with file-skeleton present. Naming goes after creation date and time.

        :param path_to_store:
        :return:
        """

        # create file name with current date stamp
        file_name = "store_{:%Y-%m-%d_%H-%M-%S}.json".format(datetime.now())
        file_path = OP.join(path_to_store, file_name)

        try:
            with open(file_path, 'w') as f:
                json.dump(self.empty_store(), f, indent=4)
        except IOError:
            logger.exception(f"Could not create file {file_path}")
        else:
            logger.debug(f"File creates successfully: {file_path}")

        return file_path


    @staticmethod
    def empty_store():
        """
        Returns empty data file template.

        :return:
        """
        return {"header":{"last_access":"", "websites":[]}, "content":{}}


    def write_jobs_to_store(self, path_to_file, websites, scraped_jobs):
        """
        This function writes jobs to data file. Jobs argument must be list of dicts.

        :param site: string defining which site is scraped
        :param args: each arg is a dict containing job description
        :return:
        """

        new_store = self.empty_store()
        new_store["header"]["last_access"] = str(datetime.now())
        new_store["header"]["websites"] = websites
        new_store["content"] = scraped_jobs

        # write postings to file
        with open(path_to_file, "w") as f:
            json.dump(new_store, f, indent=4)
            logger.info("Scraped jobs written to file: %s", path_to_file)

        return


    def get_new_removed_jobs_by_group(self, scraped, stored):
        """
        Scraped and stored jobs lists are compared to check which jobs were added or removed. Output is a dictionary
        containing the jobs sorted by "new" and "removed" per jobs-group.

        Output has the format:
        out = {group1: {added: [{job1}, {job2},...], removed: [{job1}, {job2},...]}, group2:{},...}

        :param scraped:
        :param stored:
        :return:
        """
        logger.info("Comparing scraped and stored jobs")

        all_groups = list(set().union(*(d.keys() for d in [scraped, stored])))
        add_rem = {}
        for group in all_groups:
            add_rem[group] = {}

            # check if group exists in dict; if not, use empty list
            scraped_list = scraped[group] if group in scraped else []
            stored_list = stored[group] if group in stored else []

            # compare jobs lists and return added/removed jobs for that group
            add_rem[group]["added"], add_rem[group]["removed"] = self.compare_jobs_lists(scraped_list, stored_list)

            # if no difference in old and new, remove group from dict
            if not add_rem[group]["added"] and not add_rem[group]["removed"]:
                add_rem.pop(group, None)

        return add_rem


    @staticmethod
    def compare_jobs_lists(new, old):
        """
        This function compares existing job entries with entries in the new search. Input are two lists of dicts.

        :return:
        """
        added = [d for d in new if d not in old]
        removed = [d for d in old if d not in new]

        return added, removed


