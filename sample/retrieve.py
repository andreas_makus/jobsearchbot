import logging
from time import sleep

import selenium
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from config import config, defaults
from sample.browser import Browser
from sample import toolshed as ts


# get logger
logger = logging.getLogger(config.LOGGER)


class Retriever:
    """
    Class contains all necessary functions to retrieve HTML of the relevant websites.

    """
    def __init__(self):
        self.site_groups = config.SITE_GROUPS  # Collection of site groups

        # Function Switch
        self.html_functions = {"DLR": self.get_html_DLR,
                               "AIRBUS": self.get_html_AIRBUS,
                               "FERCHAU": self.get_html_FERCHAU}


    def start_browser(self):
        """
        Starts browser and returns webdriver (browser) object.

        :return:
        """
        self.browser = Browser().start_browser(config.BROWSER)
        return self.browser


    def quit_browser(self):
        """
        Quits browser.

        :return:
        """
        self.browser.quit()
        logger.info("Browser closed")
        return


    def retrieve_site_html(self, site):
        """"""
        # check the site group (e.g. DLR, Airbus, etc)
        url_group = self.get_url_group(site)
        if not url_group:
            raise NotImplementedError

        # get html and convert to soup
        html_string = self.get_site_html(url_group, site)
        html_soup = ts.html_str_to_soup(html_string)

        return  url_group, html_soup


    def get_url_group(self, site):
        """
        Determines which group the provided url string belongs to. First match is returned. If no match, None is returned.

        :param site:
        :return:
        """
        for group, url_list in self.site_groups.items():
            for url_string in url_list:
                if url_string in site:
                    return group

        return # returns None otherwise


    def get_site_html(self, url_group, site):
        """
        Switch-function to get html from website. Different scrape functions are called depending on url group.

        For example, the DLR website needs to be retrieved differently than the AIRBUS website, and this function calls
        the correct function to use.

        :param site:
        :return:
        """
        site_html = self.html_functions[url_group](site)

        if site_html:
            logger.info("Site HTML successfully retrieved")

        return site_html


    def get_html_element_by(self, by, key):
        """
        Get elements by attribute, includes waiting times for page to load properly.

        :param by:
        :param key:
        :return:
        """
        try:
            if by == "xpath":
                element = WebDriverWait(self.browser, defaults.DELAY).until(
                    EC.presence_of_element_located((By.XPATH, key)))
                # element = self.browser.find_element_by_xpath(key)
            elif by == "class":
                element = WebDriverWait(self.browser, defaults.DELAY).until(
                    EC.presence_of_element_located((By.CLASS_NAME, key)))
                # element = self.browser.find_element_by_class_name(key)
            elif by == "id":
                element = self.browser.find_element_by_id(key)
            else:
                raise NotImplementedError
        except (NoSuchElementException, TimeoutException) as err:
            logger.error("Could not find element by %s", by)
        except NotImplementedError:
            logger.error("Can not find elements by %s; must be one of ['class', 'xpath', 'id']", by)
        else:
            return element

        return


    def get_html_DLR(self, site):
        """
        Retrieves the html of the website on the job portal. The "load more" button is pressed until no more jobs to
        load before scraping the html.

        :param driver:
        :param site:
        :return:
        """

        # get site
        logger.info("Retrieving html from %s", site)
        self.browser.get(site)

        # get "more button" and its text element to check for more jobs
        more_button_xpath = """//*[@id="jobs-content"]/div[5]/ul/li/a"""
        more_button = self.get_html_element_by("xpath", more_button_xpath)
        button_text_xpath = """//*[@id="jobs-content"]/div[5]/ul/li/a/span"""
        button_text_elem = self.get_html_element_by("xpath", button_text_xpath)

        # loop until "more button" is not clickable anymore, then return html
        while button_text_elem.text != 'No more vacancies found':
            try:
                more_button.click()
            except WebDriverException:
                logger.error("Button is not clickable: '%s'", more_button.get_attribute("title"))
            else:
                logger.info("Clicked button: '%s'", more_button.get_attribute("title"))
            finally:
                sleep(defaults.DELAY)
                button_text_elem = self.get_html_element_by("xpath", button_text_xpath)
                more_button = self.get_html_element_by("xpath", more_button_xpath)

        return self.browser.execute_script("return document.getElementsByTagName('html')[0].outerHTML")


    def get_html_AIRBUS(self, site):
        """
        Retrieves the relevant html site from the Airbus job portal.

        :param site:
        :return:
        """
        # Airbus page click() issues:
        # https://stackoverflow.com/questions/11908249/debugging-element-is-not-clickable-at-point-error
        # https://stackoverflow.com/questions/35636155/selenium-element-not-clickable-other-element-would-receive-click
        # https://stackoverflow.com/questions/34562061/webdriver-click-vs-javascript-click

        # get site
        logger.info("Retrieving %s", site)
        self.browser.get(site)

        # define and get more button
        btn_class = "btn-more-less"
        more_btn = self.get_html_element_by("class", btn_class)

        # click button until no more loads
        button_clicked = 0  # safety measure
        while more_btn:
            if button_clicked > defaults.CLICK_LIMIT:
                logger.error("Button click limit reached!")
                break

            try:
                btn_txt = more_btn.text
                more_btn.click()
            except WebDriverException:
                logger.error("Button not clickable: %s", more_btn.text)
            else:
                logger.info("Button clicked succesfully: %s", btn_txt)
            finally:
                button_clicked += 1
                sleep(defaults.DELAY)
                more_btn = self.get_html_element_by("class", btn_class)

        return self.browser.execute_script("return document.getElementsByTagName('html')[0].outerHTML")


    def get_html_FERCHAU(self, site):
        """"""

        # get site
        logger.info("Retrieving %s", site)
        self.browser.get(site)

        # define and get more button
        btn_id = "loadbutton"
        more_btn = self.get_html_element_by("id", btn_id)

        # click button until no more loads
        button_clicked = 0  # safety measure
        while more_btn.is_displayed():
            if button_clicked > defaults.CLICK_LIMIT:  # to prevent infinite clicks
                logger.error("Button click limit reached!")
                break

            try:
                # more_btn.click()
                more_btn.send_keys(selenium.webdriver.common.keys.Keys.RETURN)
            except WebDriverException:
                logger.error("Button not clickable: %s", btn_id)
            else:
                logger.info("Button clicked succesfully: %s", btn_id)
            finally:
                button_clicked += 1
                sleep(defaults.DELAY)
                more_btn = self.get_html_element_by("id", btn_id)

        return self.browser.execute_script("return document.getElementsByTagName('html')[0].outerHTML")