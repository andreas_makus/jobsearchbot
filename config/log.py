import os, logging
from logging.handlers import RotatingFileHandler
from config import config


def configure_logger(logger_name):
    """
    Configure the provided logger.

    :param logger:
    :return: logger:
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

    file_formatter = logging.Formatter("%(asctime)s    %(levelname)-7s   %(module)-14s    %(message)s",
                                       datefmt="%Y-%m-%d %H:%M:%S")
    console_formatter = logging.Formatter("%(levelname)-6s    %(module)-14s   %(message)s")

    # set file handling
    file_handler = RotatingFileHandler(config.LOG_FILE, mode="a", maxBytes=config.MAX_BYTES, backupCount=0)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(file_formatter)

    # set console handling
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(console_formatter)

    # add handlers to logger
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    return logger


def log_blank_line(lines=1):
    """
    Writes one or more blank lines to the log file.

    :param blank_lines:
    :return:
    """
    with open(config.LOG_FILE, "a") as f:
        f.write("".join("\n" for _ in range(lines)))

    return