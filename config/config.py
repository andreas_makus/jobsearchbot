import os.path as op

# define main dir
MAIN_DIR = op.dirname(op.dirname(op.abspath(__file__)))
MAIN_FILE = op.join(MAIN_DIR, "main.py")

# Database
STORE_DIR = op.join(MAIN_DIR, "data")  # TODO: remove
MONGODB = r"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"
DATABASE_DIR = op.join(MAIN_DIR, "data", "db", "jobs_database")
DATABASE = "jobs_db"
COLLECTION_PREFIX = "jobs_collection_"  # prefix of each hashed collection
HASH_SLICE = 12   # first x characters of hash are used

# Browser used for loading site (CHROME, PHANTOMJS)
BROWSER = "FIREFOX"
BROWSER_VISIBLE = 1

# logging
LOGGER = "Main_Logger"
MAX_BYTES = 1*1024*1024  # 1 MB
LOG_FILE = op.join(MAIN_DIR, "logs", "info.log")
BROWSER_LOG = op.join(MAIN_DIR, "logs", f"{BROWSER}.log")

# Define paths to browser drivers
PHANTOMJS_PATH = op.join(MAIN_DIR, "dependencies", "phantomjs.exe")
CHROME_PATH = op.join(MAIN_DIR, "dependencies", "chromedriver.exe")
FIREFOX_PATH = op.join(MAIN_DIR, "dependencies", "geckodriver.exe")

# site groups  # TODO: get rid?
SITE_GROUPS = {"DLR": ["dlr.de/dlr/jobs/"],
                "AIRBUS": ["company.airbus.com/careers/"],
               "FERCHAU":["ferchau.com/de/de/karriere/"]}

# scraper settings
REMOVECHARS = ["\xad"]  # remove characters from string
REPLACECHARS = [("ä ", "ä"), ("ö ", "ö"), ("ü ", "ü")]
IGNORE_IN_HTML= ["\n", "\t"]  # ignore these elements
