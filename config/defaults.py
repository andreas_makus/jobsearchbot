import os.path as op
from config import config

# Browser visibility


# DEFAULTS  # TODO: get rid?
DEF_WEBSITE = r"https://www.dlr.de/dlr/jobs/en/desktopdefault.aspx/tabid-10572/#Hamburg/S:96"  # DLR jobs in Hamburg

# Browser: time to wait (sec)/ click limits
TIMEOUT = 10  # for site to load
DELAY = 2  # for button/element to appear
CLICK_LIMIT = 20  # max amount of times an html element is clicked; meant to stop while loop

# Email settings
TABLE_WIDTH = 80  # %
TABLE_BOTTOM_MARGIN = 10 # px
CELL_PADDING = 5 # px
P_MARGIN = 2 # em