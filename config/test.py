import os.path as op
from config import config

TEST_DATA_DIR = op.join(config.MAIN_DIR, "test", "testdata")

EXAMPLE_PATHS = {   "DLR": op.join(TEST_DATA_DIR, "example_DLR.html"),
                    "AIRBUS": op.join(TEST_DATA_DIR, "example_AIRBUS.html"),
                    "FERCHAU": op.join(TEST_DATA_DIR, "example_FERCHAU.html")}

